  <?php 
        ini_set('display_errors',1);
        ini_set('display_startup_errors',1);
        error_reporting(E_ALL);
  ?>
  <?php 
        include_once('sev/PHP/users.php'); 
  ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
      
      <title>User Table</title>
    </head>
<body>
 <?php $Obj = new users();

        if(isset($_GET['d_id'])){
          $id=$_GET['d_id'];
          try {
            if($id>0){
              $success = $Obj->delete($id);
            }
          } catch (PDOException $e) {
            echo $e->getMessage();
          }
        }

        $datas = $Obj->runSql("SELECT * FROM users");
        $datas->execute();
       
      //  $sa = $datas->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($sa);
  ?>
  <h1 class="row justify-content-center text-danger">All Users</h1>
    <a href="sev/PHP/AddUser.php" class="btn btn-success btn-md" style="margin-left:120px;margin-bottom:10px;">New</a>
  <table class='table table-striped container'>
      <thead>
            <tr>
              <th>ID:</th>
                  <th>Name</th>
                      <th>Last Name</th>
                  <th>Company</th>
              <th>Action</th>
          </tr>
      </thead>

  <tbody>


      <?php  while($mass = $datas->fetch(PDO::FETCH_ASSOC)){ ?>
        <tr>
            <td><?php echo $mass['id']; ?></td>
                <td><?php echo $mass['name']; ?></td>
                    <td><?php echo $mass['lname']; ?></td>
                          <td><?php echo $mass['company']; ?></td>
                            <td>
                          <a href="sev/PHP/AddUser.php?edit_id=<?php echo $mass['id'];?>"><button class="btn btn-primary btn-sm">Edit</button></a>
                          <button class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#ex<?php echo $mass['id'];?>"> 
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
            </svg> </button></td>
        </tr>

    <!-- Modal -->
    <div class="modal fade" id="ex<?php echo $mass['id'];?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Warning !</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
           Are you want to delete this Id(<?php echo $mass['id'];?>)
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <a href="index.php?d_id=<?php echo $mass['id']; ?>">  <button type="button" class="btn btn-danger">Delete</button></a>
          </div>
        </div>
      </div>
    </div>


      <?php  } ?>
      
  </tbody>

</table>

</body>
</html>