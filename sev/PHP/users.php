<?php

require_once "database.php";

class users{

    private $conn;

    public function __construct(){
        $db = new database();
        $this->conn = $db->connection();

    }

    public function __destruct(){
       // $this->conn->close();
    }
    public function runSql($sql){
        return $this->conn->prepare($sql);
    }

    public function insert($name,$lname,$company){
        try{
            $dam = $this->conn->prepare("INSERT INTO users (name,lname,company) VALUES (:name,:lname,:company)");
            $dam->bindParam(':name',$name);
            $dam->bindParam(':company',$company);
            $dam->bindParam(':lname',$lname);
            $dam->execute();     
        }catch(PDOException $e){
            echo $e->getMessage();
        }
        return $dam;
    }

    public function update($name,$lname,$company,$id){
        try {
            $smtp = $this->conn->prepare("UPDATE users SET `name`=:name,lname=:lname,company=:company WHERE  id=:id");
            $smtp->bindParam(':name',$name);
            $smtp->bindParam(':lname',$lname);
            $smtp->bindParam(':company',$company);
            $smtp->bindParam(':id',$id);
            $smtp->execute();
            return $smtp;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete($id){
      try {
          $smtp = $this->conn->prepare("DELETE FROM users WHERE id=$id");
          $smtp->execute();
          $dam=['succes'=>true,'data'=>$smtp];
          return json_encode($dam);  
      } catch (PDOExcetion $e) {
          echo $e->getMessage();
      }
    }
    public function redirect($url){
        header("Location:$url");
    }
}


?>