<?php 
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);
?>

<?php
        require_once "users.php"; 

        $save = new users();
        
        if(isset($_GET['edit_id'])){
            $id = $_GET['edit_id'];

            $smtp = $save->runSql("SELECT * FROM  users WHERE id=$id");
            $smtp->execute();
            $rowUser = $smtp->fetch(PDO::FETCH_ASSOC);
        } 

        else{
            $id = null;
            $rowUser=null;
        }

    try {
      if(isset($_POST['user_save'])){
            $name = strip_tags($_POST['name']);
            $lname = strip_tags($_POST['lname']);
            $company = strip_tags($_POST['company']);
            $id = trim($_POST['id']);
            
    if($id!=''){
        if($save->update($name,$lname,$company,$id)){
             $save->redirect('../../index.php?updated');
          }
    }else 
        if($save->insert($name,$lname,$company)){
           $save->redirect('../../index.php?inserted');
    }else{
          $save->redirect('AddUser.php?error');
           }
        }
     } 
    catch (PDOExcetion $e) {
         echo $e->getMessage();
    }


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <h3 class="row justify-content-center text-danger mt-5">User Data :</h3>
    <form action="AddUser.php" method="POST">
    <div class="container mt-5">
        <div >
             <!-- <label class="form-label"for="id">ID:</label> -->
            <input class="form-control" type="hidden" id="id" name="id" value="<?php if(isset($rowUser['id'])) echo $rowUser['id']; ?>" readonly>
        </div>
    
    <div class="mb-3">
        <div >
             <label class="form-label"for="name">NAME *</label>
            <input class="form-control" type="text" placeholder="First Name" id="name"  name="name" value="<?php if(isset($rowUser['name']))echo $rowUser['name']; ?>">
        </div>
    </div>
    <div class="mb-3">
        <div >
             <label class="form-label"for="lname">LAST NAME *</label>
            <input class="form-control" type="text" id="lname" placeholder="Last Name" name="lname" value="<?php if(isset($rowUser['lname'])) echo $rowUser['lname']; ?>" >
        </div>
    </div>
    <div class="mb-3">
        <div >
             <label class="form-label"for="comapny">Comapny </label>
            <input class="form-control" type="text" placeholder="Comapny" id="comapny" name="company" value="<?php  if(isset($rowUser['company']))echo $rowUser['company']; ?>">
        </div>
    </div>
    <input type="submit" value="Submit" name="user_save" class="btn btn-primary">
    </div>
    </form>
 
</body>
</html>



